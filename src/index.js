import Vue from 'vue';
import Main from './app/Main.vue';
Vue.use(require('vue-moment'));

import './index.scss';

export default new Vue({
  el: '#root',
  render: h => h(Main)
});
